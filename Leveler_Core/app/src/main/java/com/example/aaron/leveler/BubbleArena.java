package com.example.aaron.leveler;

import android.content.Context;
import android.graphics.Canvas;

/**
 *
 */

public class BubbleArena {

    private Bubble bubble;

    public BubbleArena() {

        bubble = new Bubble();

    }

    public void update(int width, int height, float x, float y){

        bubble.move(0, 0, width, height, x, y);
    }

    public void draw(Canvas canvas){

        canvas.drawRGB(156, 174, 216);

        bubble.draw(canvas);
    }
}
