package com.example.aaron.leveler;

import android.content.Context;
import android.graphics.Canvas;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.SurfaceHolder;

/**
 *
 */

public class BubbleThread extends Thread{

    private SurfaceHolder surfaceHolder;
    private BubbleArena bubbleArena;
    private boolean isRunning;
    private float x, y;

    private SensorManager mSensorManager;

    public BubbleThread(SurfaceHolder sh, Context context) {

        this.isRunning = true;
        this.surfaceHolder = sh;
        this.bubbleArena = new BubbleArena();

        x = 0.0f;
        y = 0.0f;
    }

    public void run(){

        try{
            while(isRunning){

                Canvas canvas = surfaceHolder.lockCanvas();
                bubbleArena.update(canvas.getWidth(), canvas.getHeight(), x, y);
                bubbleArena.draw(canvas);

                surfaceHolder.unlockCanvasAndPost(canvas);
            }
        } catch (NullPointerException e){

            e.printStackTrace();
        }
    }

    public void setPosition(float x, float y){

        this.x = x;
        this.y = y;
    }

    public void endBubble() {
        isRunning = false;
    }
}
