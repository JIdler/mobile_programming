package com.example.aaron.leveler;

import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 *
 */

public class BubbleSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

    private BubbleThread bubbleThread;

    public BubbleSurfaceView(Context context, AttributeSet attrs){

        super(context, attrs);

        SurfaceHolder holder = getHolder();
        holder.addCallback(this);

        // create new thread
        bubbleThread = new BubbleThread(holder, context);
    }

    public void surfaceCreated(SurfaceHolder holder){

        bubbleThread.start();
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height){

    }

    public void surfaceDestroyed(SurfaceHolder holder){

        bubbleThread.endBubble();
        Thread dummyThread = bubbleThread;
        bubbleThread = null;
        dummyThread.interrupt();
    }

    public void setBubbleXY(float x, float y){

        bubbleThread.setPosition(x, y);
    }
}
