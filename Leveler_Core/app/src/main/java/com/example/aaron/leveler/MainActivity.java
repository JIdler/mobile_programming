package com.example.aaron.leveler;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends Activity implements SensorEventListener {
    private SensorManager mSensorManager;
    private Sensor tilt;
    private TextView top;
    private TextView bottom;
    private TextView left;
    private TextView right;

    BubbleSurfaceView bubbleSurfaceView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        top     = (TextView)findViewById(R.id.topText);
        bottom  = (TextView)findViewById(R.id.bottomText);
        left    = (TextView)findViewById(R.id.leftText);
        right   = (TextView)findViewById(R.id.rightText);
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        tilt = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);


        // bubble stuff
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.bubbleArena);

        bubbleSurfaceView = new BubbleSurfaceView(this, null);
        frameLayout.addView(bubbleSurfaceView);

    }

    @Override
    public final void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Do something here if sensor accuracy changes.
    }

    @Override
    public final void onSensorChanged(SensorEvent event) {

        // 3 values, one for each axis.
        float xTilt = event.values[0];
        float yTilt = event.values[1];
        float zTilt = event.values[2];
        int orientation = getResources().getConfiguration().orientation;
        int rotation = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation();

        if(orientation == Configuration.ORIENTATION_PORTRAIT) {

            if (rotation == Surface.ROTATION_180) {

                showTilt(xTilt, -yTilt);
                bubbleSurfaceView.setBubbleXY(-xTilt, -yTilt);
            }
            else {

                showTilt(xTilt, yTilt);
                bubbleSurfaceView.setBubbleXY(xTilt, yTilt);
            }

        } else {

            if(rotation == Surface.ROTATION_90) {

                showTilt(-yTilt, xTilt);
                bubbleSurfaceView.setBubbleXY(-yTilt, xTilt);
            }
            else {

                showTilt(yTilt, -xTilt);
                bubbleSurfaceView.setBubbleXY(yTilt, -xTilt);
            }

        }

        // Do stuff if device is standing up and level
        notifyLevel(zTilt);

   //     showTilt(xTilt, yTilt);
        Log.d("Sensor Changed", String.format("x = %8.6f,  y = %8.6f,  z = %8.6f",xTilt, yTilt, zTilt));
    }

    public void showTilt(float hTilt, float vTilt) {
        top.setText("" + vTilt);
        bottom.setText("" + -vTilt);
        left.setText("" + -hTilt);
        right.setText("" + hTilt);
    }

    // Checks if the device is level when standing up
    // and changes the background color depending on if level or not
    public void notifyLevel(float zTilt){

        RelativeLayout layout = (RelativeLayout) findViewById(R.id.mainLayout);
        TextView message = (TextView) findViewById(R.id.message);

        if((int)zTilt == 0){

            // set layout background to green
            layout.setBackgroundColor(Color.GREEN);

            message.setVisibility(View.VISIBLE);

        } //else if((zTilt < 2 && zTilt > 1) || (zTilt > -2 && zTilt < -1))

        else {

            // set layout background to white
            layout.setBackgroundColor(Color.WHITE);

            message.setVisibility(View.GONE);

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, tilt,
                SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
