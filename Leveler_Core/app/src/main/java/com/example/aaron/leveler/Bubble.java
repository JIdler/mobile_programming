package com.example.aaron.leveler;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

/**
 *
 */

public class Bubble {

    private final int RADIUS = 100;
    private final int REVERSE = -1;
    private int x; // what is this?
    private int y; // and this?


    // Constructor
    public Bubble(){

        x = 700;
        y = 1050;
    }

public void move(int leftWall, int topWall, int rightWall, int bottomWall, float velX, float velY){

    x += velX;
    y += (velY * REVERSE);

    if(y > bottomWall - RADIUS){
        y = bottomWall - RADIUS;
    } else if(y < topWall + RADIUS){
        y = topWall + RADIUS;
    }

    if(x > rightWall - RADIUS){
        x = rightWall - RADIUS;
    } else if(x < leftWall + RADIUS){
        x = leftWall + RADIUS;
    }
}

    public void draw(Canvas canvas) {

        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        canvas.drawCircle(x, y, RADIUS, paint);

    }
}
