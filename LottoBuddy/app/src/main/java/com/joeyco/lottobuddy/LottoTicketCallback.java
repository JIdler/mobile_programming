package com.joeyco.lottobuddy;

/**
 * Joey Idler
 */

public interface LottoTicketCallback {

    public void onLineAdded(LottoTicket ticket);
}
