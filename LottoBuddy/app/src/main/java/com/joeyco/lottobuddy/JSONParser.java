package com.joeyco.lottobuddy;

import android.util.JsonReader;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Joey Idler
 *
 *
 */

public class JSONParser {

    private static final String TAG = JSONParser.class.getSimpleName();

    public JSONParser() {

    }

    public List<WinningNumbersData> readJsonStream(InputStream input) throws IOException {

        JsonReader reader = new JsonReader(new InputStreamReader(input, "UTF-8"));;

        try {
            return readLottoDataArray(reader);
        } finally {
            reader.close();
        }
    }

    private List<WinningNumbersData> readLottoDataArray(JsonReader reader) throws IOException {

        List<WinningNumbersData> winningNumbers = new ArrayList<>();

        reader.beginArray();
        while (reader.hasNext()) {
            winningNumbers.add(readData(reader));
        }
        reader.endArray();

        return winningNumbers;
    }

    private WinningNumbersData readData(JsonReader reader) throws IOException {

        String drawDate = null;
        List<Integer> winningNumbers = null;
        int multiplier = -1;

        reader.beginObject();
        while (reader.hasNext()) {

            String name = reader.nextName();

            if (name.equals("draw_date")) {
                drawDate = reader.nextString();
            } else if (name.equals("winning_numbers")) {
                winningNumbers = numbersStringToIntList(reader.nextString());
            } else if (name.equals("multiplier")) {
                multiplier = Integer.parseInt(reader.nextString());
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();

        return new WinningNumbersData(drawDate, winningNumbers, multiplier);
    }

    private List<Integer> numbersStringToIntList(String numbers) {

        List<Integer> winningNumbers = new ArrayList<>();

        for (String num: numbers.split(" ")) {
            winningNumbers.add(Integer.parseInt(num));
        }

        return winningNumbers;
    }
}
