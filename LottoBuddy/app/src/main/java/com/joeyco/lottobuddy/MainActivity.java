package com.joeyco.lottobuddy;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.common.api.CommonStatusCodes;

import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 *  Joey Idler
 *
 */

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final int LOTTO_NUMBER_CAPTURE = 9003;

    // Intent request code to handle updating play services if needed.
    private static final int RC_HANDLE_GMS = 9001;

    // Permission request codes need to be < 256
    private static final int RC_HANDLE_CAMERA_PERM = 2;

    private static final String SAVED_LIST_KEY = "winningNumbersList";

    // JSON url's
    private final String powerballURL = "http://data.ny.gov/resource/d6yy-54nr.json";

    // Buttons
    private Button scanTicketButton;

    // TextViews
    private TextView estimatedPayout;
    private TextView winningNumbersDate;
    private TextView multiplier;
    private EditText winningNumber1;
    private EditText winningNumber2;
    private EditText winningNumber3;
    private EditText winningNumber4;
    private EditText winningNumber5;
    private EditText winningNumber6;
    
    private EditText lottoTicketNumberA1;
    private EditText lottoTicketNumberA2;
    private EditText lottoTicketNumberA3;
    private EditText lottoTicketNumberA4;
    private EditText lottoTicketNumberA5;
    private EditText lottoTicketNumberA6;
    private EditText lottoTicketNumberB1;
    private EditText lottoTicketNumberB2;
    private EditText lottoTicketNumberB3;
    private EditText lottoTicketNumberB4;
    private EditText lottoTicketNumberB5;
    private EditText lottoTicketNumberB6;
    private EditText lottoTicketNumberC1;
    private EditText lottoTicketNumberC2;
    private EditText lottoTicketNumberC3;
    private EditText lottoTicketNumberC4;
    private EditText lottoTicketNumberC5;
    private EditText lottoTicketNumberC6;
    private EditText lottoTicketNumberD1;
    private EditText lottoTicketNumberD2;
    private EditText lottoTicketNumberD3;
    private EditText lottoTicketNumberD4;
    private EditText lottoTicketNumberD5;
    private EditText lottoTicketNumberD6;
    private EditText lottoTicketNumberE1;
    private EditText lottoTicketNumberE2;
    private EditText lottoTicketNumberE3;
    private EditText lottoTicketNumberE4;
    private EditText lottoTicketNumberE5;
    private EditText lottoTicketNumberE6;


    // ListView stuff
    private ListView playerDrawLines;
    private ArrayAdapter<String> drawLinesAdapter;

    // Lists
    private List<WinningNumbersData> winningNumbersList;
    private HashMap<String, List<Integer>> lottoTicketLines;

    LottoTicket lottoTicket;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        initViews();

        new GetWinningNumbers().execute();

        scanTicketButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(),
                                            LottoTicketScannerActivity.class);

                startActivityForResult(intent, LOTTO_NUMBER_CAPTURE);
            }
        });
    }

    private void initViews() {

        estimatedPayout = (TextView) findViewById(R.id.estimated_prize);

        scanTicketButton = (Button) findViewById(R.id.scan_ticket_button);

        winningNumbersDate = (TextView) findViewById(R.id.winning_numbers_date);

        multiplier = (TextView) findViewById(R.id.multiplier);

        winningNumber1 = (EditText) findViewById(R.id.winning_num1);
        winningNumber2 = (EditText) findViewById(R.id.winning_num2);
        winningNumber3 = (EditText) findViewById(R.id.winning_num3);
        winningNumber4 = (EditText) findViewById(R.id.winning_num4);
        winningNumber5 = (EditText) findViewById(R.id.winning_num5);
        winningNumber6 = (EditText) findViewById(R.id.winning_num6);
        
        lottoTicketNumberA1 = (EditText) findViewById(R.id.number_A1);
        lottoTicketNumberA2 = (EditText) findViewById(R.id.number_A2);
        lottoTicketNumberA3 = (EditText) findViewById(R.id.number_A3);
        lottoTicketNumberA4 = (EditText) findViewById(R.id.number_A4);
        lottoTicketNumberA5 = (EditText) findViewById(R.id.number_A5);
        lottoTicketNumberA6 = (EditText) findViewById(R.id.number_A6);
        lottoTicketNumberB1 = (EditText) findViewById(R.id.number_B1);
        lottoTicketNumberB2 = (EditText) findViewById(R.id.number_B2);
        lottoTicketNumberB3 = (EditText) findViewById(R.id.number_B3);
        lottoTicketNumberB4 = (EditText) findViewById(R.id.number_B4);
        lottoTicketNumberB5 = (EditText) findViewById(R.id.number_B5);
        lottoTicketNumberB6 = (EditText) findViewById(R.id.number_B6);
        lottoTicketNumberC1 = (EditText) findViewById(R.id.number_C1);
        lottoTicketNumberC2 = (EditText) findViewById(R.id.number_C2);
        lottoTicketNumberC3 = (EditText) findViewById(R.id.number_C3);
        lottoTicketNumberC4 = (EditText) findViewById(R.id.number_C4);
        lottoTicketNumberC5 = (EditText) findViewById(R.id.number_C5);
        lottoTicketNumberC6 = (EditText) findViewById(R.id.number_C6);
        lottoTicketNumberD1 = (EditText) findViewById(R.id.number_D1);
        lottoTicketNumberD2 = (EditText) findViewById(R.id.number_D2);
        lottoTicketNumberD3 = (EditText) findViewById(R.id.number_D3);
        lottoTicketNumberD4 = (EditText) findViewById(R.id.number_D4);
        lottoTicketNumberD5 = (EditText) findViewById(R.id.number_D5);
        lottoTicketNumberD6 = (EditText) findViewById(R.id.number_D6);
        lottoTicketNumberE1 = (EditText) findViewById(R.id.number_E1);
        lottoTicketNumberE2 = (EditText) findViewById(R.id.number_E2);
        lottoTicketNumberE3 = (EditText) findViewById(R.id.number_E3);
        lottoTicketNumberE4 = (EditText) findViewById(R.id.number_E4);
        lottoTicketNumberE5 = (EditText) findViewById(R.id.number_E5);
        lottoTicketNumberE6 = (EditText) findViewById(R.id.number_E6);
    }
    
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        savedInstanceState.putParcelableArrayList(SAVED_LIST_KEY,
                                            (ArrayList<? extends Parcelable>) winningNumbersList);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {

        super.onRestoreInstanceState(savedInstanceState);

        winningNumbersList = savedInstanceState.getParcelableArrayList(SAVED_LIST_KEY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == LOTTO_NUMBER_CAPTURE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                   //TODO: use try catch statements to handle lottoTicketLines being null
                    try {

                        lottoTicketLines = (HashMap<String, List<Integer>>)
                                                        data.getSerializableExtra("linesHashmap");

                        lottoTicket = new LottoTicket();
                        lottoTicket.setCallback(new LottoTicketListener());
                        lottoTicket.addAllLines(lottoTicketLines);

                        setLottoTicketLines();

                        setEstimatedPayout();

                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                } else {
                    //TODO: handle no text being captured from ticket here
                    Log.d(TAG, "No Text captured, intent data is null");
                }
            } else {
                //TODO:handle resultCode that isn't SUCCESS here
            }
        }
        else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void setLottoTicketLines() {
        
        for (String lineID : lottoTicketLines.keySet()) {
            
            List<Integer> numbers = lottoTicketLines.get(lineID);
            
            if (lineID.equals("A")) {

                lottoTicketNumberA1.setText(Integer.toString(numbers.get(0)));
                lottoTicketNumberA2.setText(Integer.toString(numbers.get(1)));
                lottoTicketNumberA3.setText(Integer.toString(numbers.get(2)));
                lottoTicketNumberA4.setText(Integer.toString(numbers.get(3)));
                lottoTicketNumberA5.setText(Integer.toString(numbers.get(4)));
                lottoTicketNumberA6.setText(Integer.toString(numbers.get(5)));
            } else if (lineID.equals("B")) {

                lottoTicketNumberB1.setText(Integer.toString(numbers.get(0)));
                lottoTicketNumberB2.setText(Integer.toString(numbers.get(1)));
                lottoTicketNumberB3.setText(Integer.toString(numbers.get(2)));
                lottoTicketNumberB4.setText(Integer.toString(numbers.get(3)));
                lottoTicketNumberB5.setText(Integer.toString(numbers.get(4)));
                lottoTicketNumberB6.setText(Integer.toString(numbers.get(5)));
            } else if (lineID.equals("C")) {

                lottoTicketNumberC1.setText(Integer.toString(numbers.get(0)));
                lottoTicketNumberC2.setText(Integer.toString(numbers.get(1)));
                lottoTicketNumberC3.setText(Integer.toString(numbers.get(2)));
                lottoTicketNumberC4.setText(Integer.toString(numbers.get(3)));
                lottoTicketNumberC5.setText(Integer.toString(numbers.get(4)));
                lottoTicketNumberC6.setText(Integer.toString(numbers.get(5)));
            } else if (lineID.equals("D")) {

                lottoTicketNumberD1.setText(Integer.toString(numbers.get(0)));
                lottoTicketNumberD2.setText(Integer.toString(numbers.get(1)));
                lottoTicketNumberD3.setText(Integer.toString(numbers.get(2)));
                lottoTicketNumberD4.setText(Integer.toString(numbers.get(3)));
                lottoTicketNumberD5.setText(Integer.toString(numbers.get(4)));
                lottoTicketNumberD6.setText(Integer.toString(numbers.get(5)));
            } else if (lineID.equals("E")) {

                lottoTicketNumberE1.setText(Integer.toString(numbers.get(0)));
                lottoTicketNumberE2.setText(Integer.toString(numbers.get(1)));
                lottoTicketNumberE3.setText(Integer.toString(numbers.get(2)));
                lottoTicketNumberE4.setText(Integer.toString(numbers.get(3)));
                lottoTicketNumberE5.setText(Integer.toString(numbers.get(4)));
                lottoTicketNumberE6.setText(Integer.toString(numbers.get(5)));
            }
        }
    }
    
    private void setEstimatedPayout() {

        try {

            double payout = lottoTicket.getEstimatedPayout(winningNumbersList.get(0));
            
            NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();

            estimatedPayout.setText("  " + currencyFormatter.format(payout));

            Log.i(TAG, "Estimated Payout: " + currencyFormatter.format(payout));
        } catch (NullPointerException e) {
            Log.w(TAG, "winningNumbersList is null");
        }
    }

    /**************************************************************************************/

    public class LottoTicketListener implements LottoTicketCallback {

        @Override
        public void onLineAdded(LottoTicket lottoTicket) {


        }
    }

    /**************************************************************************************/

    /**
     * Based on code from https://www.tutorialspoint.com/android/android_json_parser.htm
     */
    private class GetWinningNumbers extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... arg0) {

            HttpHandler http = new HttpHandler();
            InputStream input = http.makeServiceCall(powerballURL);

            JSONParser jsonParser = new JSONParser();

            try {
                winningNumbersList = jsonParser.readJsonStream(input);
            } catch (IOException e) {
                Log.e(TAG, "IOException: " + e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            WinningNumbersData currWinningNumsData = winningNumbersList.get(0);

            winningNumbersDate.setText(" " + currWinningNumsData.getDrawDate());

            multiplier.setText(" " + currWinningNumsData.getMultiplier());

            winningNumber1.setText(Integer.toString(currWinningNumsData.getWinningNumberAtPosition(1)));
            winningNumber2.setText(Integer.toString(currWinningNumsData.getWinningNumberAtPosition(2)));
            winningNumber3.setText(Integer.toString(currWinningNumsData.getWinningNumberAtPosition(3)));
            winningNumber4.setText(Integer.toString(currWinningNumsData.getWinningNumberAtPosition(4)));
            winningNumber5.setText(Integer.toString(currWinningNumsData.getWinningNumberAtPosition(5)));
            winningNumber6.setText(Integer.toString(currWinningNumsData.getWinningNumberAtPosition(6)));
        }
    }
}
