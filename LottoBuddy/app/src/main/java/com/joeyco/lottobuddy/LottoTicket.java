package com.joeyco.lottobuddy;

import android.util.Log;

import com.google.android.gms.vision.text.Text;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * Joey Idler
 */

public class LottoTicket {

    private static final String TAG = LottoTicket.class.getSimpleName();

    private LottoTicketCallback callback;

    private List<TicketLine> lines;

    private HashMap<String, List<Integer>> linesMap;

    public LottoTicket() {

        this.lines = new ArrayList<TicketLine>();
        this.linesMap = new HashMap<String, List<Integer>>();
    }

    public void addAllLines(Collection<? extends Text> lines) {

        for (Text line: lines)
            this.addLine(line);
    }

    public void addAllLines(HashMap<String, List<Integer>> linesMap) {

        this.linesMap = linesMap;

        for (String lineID: this.linesMap.keySet())
            this.addLine(this.linesMap.get(lineID));
    }

    public void addLine(Text line) {

        this.lines.add(new TicketLine(line));

        Log.i(TAG, "Adding line to lotto ticket: " + line.getValue());

        if (this.callback != null)
            this.callback.onLineAdded(this);
    }

    public void addLine(List<Integer> line) {

        this.lines.add(new TicketLine(line));
    }

    public HashMap<String, List<Integer>> getLinesHashmap() {

        return this.linesMap;
    }

    public double getEstimatedPayout(WinningNumbersData winningNumbersData) {

        double payout = 0;

        for (TicketLine line: this.lines) {

            double temp = line.getPayout(winningNumbersData);

            if (temp == Double.MAX_VALUE)
                return temp;

            payout += temp;
        }

        return payout;
    }

    public int numberOfLines() {
        return this.lines.size();
    }

    public List<TicketLine> getLines() {

        return this.lines;
    }

    public List<String> toStringList() {

        List<String> linesAsStrings = new ArrayList<String>();

        for (TicketLine line: this.lines)
            linesAsStrings.add(line.toString());

        return linesAsStrings;
    }

    public void setCallback(LottoTicketCallback callback) {
        this.callback = callback;
    }

    /***************************************************************************/

    protected class TicketLine {

        private List<Integer> lineNumbers = new ArrayList<Integer>();

        private int powerball;

        public TicketLine(Text line) {

//            this.lineNumbers = new ArrayList<Integer>();

            extractNumbersFromLine(line);
        }

        public TicketLine(List<Integer> line) {

            lineNumbers = line;

            powerball = lineNumbers.get(lineNumbers.size() - 1);
        }

        private void extractNumbersFromLine(Text line) {

            String lineID = null;

            for (String element: line.getValue().split(" ")) {

                if (LottoUtil.isInteger(element)) {

                    this.lineNumbers.add(Integer.parseInt(element));
                } else if (element.matches("[ABCDE]")) {

                    lineID = element;
                }
            }

            linesMap.put(lineID, this.lineNumbers);

            this.powerball = this.lineNumbers.get(this.lineNumbers.size() - 1);
        }

        private void extractNumbersFromLine(String line) {

            String lineID = null;

            for (String element: line.split(" ")) {

                if (LottoUtil.isInteger(element)) {

                    this.lineNumbers.add(Integer.parseInt(element));
                } else if (element.matches("[ABCDE]")) {

                    lineID = element;
                }
            }

            linesMap.put(lineID, this.lineNumbers);

            this.powerball = this.lineNumbers.get(this.lineNumbers.size() - 1);
        }

        public double getPayout(WinningNumbersData winningNumbersData) {

            double payout = 0;
            int winningPowerball = winningNumbersData.getPowerball();

            List<Integer> winningNumbers = winningNumbersData.getWinningNumbers();

            List<Integer> matchingNumbers = new ArrayList<Integer>(lineNumbers);
            matchingNumbers.retainAll(winningNumbers);

            if (powerball == winningPowerball) {

                switch (matchingNumbers.size()) {
                    case 2:
                        payout = 4;
                        break;
                    case 3:
                        payout = 7;
                        break;
                    case 4:
                        payout = 100;
                        break;
                    case 5:
                        payout = 50000;
                        break;
                    case 6:
                        payout = Double.MAX_VALUE;
                        break;
                    default:
                        payout = 4;
                        break;
                }
            } else {

                switch (matchingNumbers.size()) {
                    case 3:
                        payout = 7;
                        break;
                    case 4:
                        payout = 100;
                        break;
                    case 5:
                        payout = 1000000;
                        break;
                    default:
                        payout = 0;
                        break;
                }
            }

            return payout;
        }

        public String toString() {

            String line = "";

            for (Integer number: lineNumbers)
                line += number + " ";

            return line;
        }
    }
}
