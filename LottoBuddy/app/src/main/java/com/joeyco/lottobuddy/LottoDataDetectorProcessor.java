package com.joeyco.lottobuddy;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.util.SparseArray;

import com.android.internal.util.Predicate;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.text.Line;
import com.google.android.gms.vision.text.Text;
import com.google.android.gms.vision.text.TextBlock;

import java.util.ArrayList;
import java.util.List;

/**
 * Joey Idler
 *
 * Based on Google mobile vision sample code.
 * Source: https://github.com/googlesamples/android-vision/blob/master/visionSamples/
 *
 *
 */

public class LottoDataDetectorProcessor implements Detector.Processor<TextBlock> {

    private static final String TAG = LottoDataDetectorProcessor.class.getSimpleName();

    private final int numElementsInLine = 9;
    private final int numIntegersInLine = 6;

    private GraphicOverlay<RecognizedTextGraphic> graphicOverlay;
    private LottoTicket lottoTicket;

    public LottoDataDetectorProcessor(GraphicOverlay<RecognizedTextGraphic> graphicOverlay,
                                                                    LottoTicket lottoTicket) {

        this.graphicOverlay = graphicOverlay;
        this.lottoTicket = lottoTicket;
    }

    @Override
    public void receiveDetections(Detector.Detections<TextBlock> detections) {

        graphicOverlay.clear();

        SparseArray<TextBlock> items = detections.getDetectedItems();

        for(int i = 0; i < items.size(); i++) {

            TextBlock item = items.valueAt(i);

            if (item != null && item.getValue() != null) {

                List<? extends Text> lines = extractDrawLines(item);

                for (Text line: lines) {

                    RecognizedTextGraphic graphic =
                            new RecognizedTextGraphic(graphicOverlay, line);

                    graphicOverlay.add(graphic);

                    lottoTicket.addLine(line);
                }
            }
        }
    }

    @Override
    public void release() {

        graphicOverlay.clear();
    }

    private List<? extends Text> extractDrawLines(TextBlock textBlock) {

        List<? extends Text> lines = textBlock.getComponents();

        List<Text> drawLines = new ArrayList<>();

        for (Text line: lines) {

            List<? extends Text> elements = line.getComponents();

            if (elements.size() == numElementsInLine) {

                String lineIdElement = elements.get(0).getValue();
                String gameIdElement = elements.get(6).getValue();
                String QPElement = elements.get(8).getValue();

                if (lineIdElement.matches("[ABCDE]") && gameIdElement.matches("[A-Z][A-Z]") &&
                        QPElement.matches("[A-Z][A-Z]")) {

                    List<String> integerElements = new ArrayList<>();

                    for (Text element : elements) {

                        String stringElement = element.getValue();

                        if (isInteger(stringElement) && stringElement.length() == 2) {

                            integerElements.add(stringElement);
                        }
                    }

                    if (integerElements.size() == numIntegersInLine) {

//                        Log.i(TAG, "Adding detected draw line: " + line.getValue());
                        drawLines.add(line);
                    }
                }
            }
        }

        return drawLines;
    }

    private boolean isInteger(String s) {

        try {

            Integer.parseInt(s);

            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
