package com.joeyco.lottobuddy;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Joey Idler
 */

public class WinningNumbersData implements Parcelable {

    private String drawDate;
    private List<Integer> winningNumbers;
    private int multiplier;

    public WinningNumbersData(String drawDate, List<Integer> winningNumbers,
                              int multiplier) {

        this.drawDate = drawDate;
        this.winningNumbers = winningNumbers;
        this.multiplier = multiplier;
    }

    public String getDrawDate() {

        return drawDate.split("T")[0];
    }

    public List<Integer> getWinningNumbers() {

        return winningNumbers;
    }

    public int getWinningNumberAtPosition(int position) {

        int number = 0;

        try {
            number = winningNumbers.get(position - 1);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        return number;
    }

    public int getPowerball() {

        return winningNumbers.get(winningNumbers.size()-1);
    }

    public int getMultiplier() {

        return multiplier;
    }

    protected WinningNumbersData(Parcel in) {
        drawDate = in.readString();
        if (in.readByte() == 0x01) {
            winningNumbers = new ArrayList<Integer>();
            in.readList(winningNumbers, Integer.class.getClassLoader());
        } else {
            winningNumbers = null;
        }
        multiplier = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(drawDate);
        if (winningNumbers == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(winningNumbers);
        }
        dest.writeInt(multiplier);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<WinningNumbersData> CREATOR = new Parcelable.Creator<WinningNumbersData>() {
        @Override
        public WinningNumbersData createFromParcel(Parcel in) {
            return new WinningNumbersData(in);
        }

        @Override
        public WinningNumbersData[] newArray(int size) {
            return new WinningNumbersData[size];
        }
    };
}
