package com.joeyco.lottobuddy;

/**
 * Joey Idler
 */

public class LottoUtil {

    public static boolean isInteger(String s) {

        try {

            Integer.parseInt(s);

            return true;
        } catch (NumberFormatException e) {

            return false;
        }
    }
}
