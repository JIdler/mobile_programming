package com.joeyco.distancerecorder;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 * Joey Idler
 */

public class GPS implements GoogleApiClient.ConnectionCallbacks,
                            GoogleApiClient.OnConnectionFailedListener, LocationListener {

    // Google Play Services Resolution Request intent identifier
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

    // Speed at which to update the location in milliseconds
    private static final long UPDATE_INTERVAL = 10000; // 10 seconds

    // Meters in a foot. For conversions to feet
    private static final float METERS_IN_FOOT = 3.28084f;

    // Context of Activity that this GPS object belongs to
    private Context context;

    // GoogleApiClient so FusedLocationApi can be utilized
    private GoogleApiClient googleApiClient;

    // Various Location objects that are needed
    private Location currentLocation;
    private LocationRequest locationRequest;

    // Keep record of the total distance traveled
    private double distance = 0.0;

    // for handling the listener to the parent class
    private OnLocationUpdatedListener locationUpdatedListener;

    /**
     * Interface for parent class to implement in order
     * for the parent class to receive locations as they are updated
     * as well as the distance as it is updated
     */
    public interface OnLocationUpdatedListener {
        public void onLocationUpdated(Location location);

        public void onDistanceUpdated(double totalDistance);
    }

    public GPS(final Context context){

        this.context = context;

        onAttach((Activity) context);

        // Check if Google Play services if available
        // before building GoogleApiClient
        if(checkPlayServices()){

            // build GoogleApiClient
            buildGoogleApiClient();

            // Set up the LocationRequest object
            buildLocationRequest();
        }
    }

    /**
     * Make sure parent activity implements OnLocationUpdatedListener()
     */
    private void onAttach(Activity activity){

        try{

            locationUpdatedListener = (OnLocationUpdatedListener) activity;
        } catch(ClassCastException e) {

            throw new ClassCastException(activity.toString() +
                " must implement GPS.OnLocationUpdatedListener");
        }
    }

    /**
     * Start Overriden GoogleApiClient callback methods
     *
     */
    @Override
    public void onConnected(Bundle unused){

    }

    @Override
    public void onConnectionSuspended(int unused){

        connectGoogleApiClient();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result){

        Log.i(GPS.class.getSimpleName(), "Connection Failed: "
                + result.getErrorMessage());
    }

    @Override
    public void onLocationChanged(Location newLocation){

        if(currentLocation == null){

            currentLocation = newLocation;
        } else {

            double startLat = currentLocation.getLatitude();
            double startLon = currentLocation.getLongitude();
            double currentLat = newLocation.getLatitude();
            double currentlon = newLocation.getLongitude();
            float[] result = new float[1]; // holds the distance between last location and new location

            // Get distance traveled between locations
            Location.distanceBetween(startLat, startLon, currentLat, currentlon, result);

            Log.i(MainActivity.class.getSimpleName(), "startLat: " + startLat + " startLon: " +
                    startLon + " currentLat: " + currentLat + " currentLon: " + currentlon + " result: " + result[0]);

            // Convert distance from meters to feet
            result[0] = result[0] * METERS_IN_FOOT;

            // Add this distance to total distance
            distance += result[0];

            // update current location
            currentLocation = newLocation;
        }

        // notify parent class of new location and total distance
        locationUpdatedListener.onLocationUpdated(currentLocation);

        locationUpdatedListener.onDistanceUpdated(distance);
    }

    /**
     * End Overridden GoogleApiClient callback methods and
     * LocationListener method
      */

    /**
     * Build GoogleApiClient object
     *
     * from http://www.androidhive.info/2015/02/android-location-api-using-google-play-services/
     */
    protected synchronized void buildGoogleApiClient(){

        googleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    /**
     * Build LocationRequest
     */
    private void buildLocationRequest(){

        locationRequest = new LocationRequest()
                .setInterval(UPDATE_INTERVAL)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Check if Google Play Services is available on device
     *
     * Slightly modified version of code found here:
     *  http://www.androidhive.info/2015/02/android-location-api-using-google-play-services/
     *
     */
    private boolean checkPlayServices(){

        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int isAvailableResult = googleApiAvailability.isGooglePlayServicesAvailable(context);

        if(isAvailableResult != ConnectionResult.SUCCESS){

            if(googleApiAvailability.isUserResolvableError(isAvailableResult)){

                googleApiAvailability.getErrorDialog((Activity) context, isAvailableResult,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }

            return false;
        }

        return true;
    }

    /**
     *  Connects GoogleApiClient
     */
    public void connectGoogleApiClient(){

        if(googleApiClient != null)
            googleApiClient.connect();
    }

    /**
     * Disconnects GoogleApiClient
     */
    public void disconnectGoogleApiClient(){

        if(googleApiClient.isConnected())
            googleApiClient.disconnect();
    }

    /**
     * Start getting location updates
     */
    public void startLocationUpdates(){

        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient,
                locationRequest, this);
    }

    /**
     * Stop getting location updates
     */
    public void stopLocationUpdates(){

        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
    }

    /**
     * Resume getting location updates
     */
    public void resumeLocationUpdates(){

        checkPlayServices();

        if(googleApiClient.isConnected())
            startLocationUpdates();
    }

    /**
     * Pause getting location updates
     */
    public void pauseLocationUpdates(){

        stopLocationUpdates();
    }

    /**
     * Reset total distance traveled to 0
     */
    public void resetDistance(){

        distance = 0.0;

        locationUpdatedListener.onDistanceUpdated(distance);
    }

}
