package com.joeyco.distancerecorder;

import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

// TODO: 10/18/16 Work on getting update speed from user 

/**
 * Joey Idler
 */


public class MainActivity extends Activity implements GPS.OnLocationUpdatedListener {
    
    // Layout Items
    private TextView latitudeText, longitudeText, totalDistanceText, accuracyText;
    private Button startStopButton;

    // GPS Object for getting location
    private GPS gps;

    // Flag to set location update requests on/off
    private boolean isRequestingLocationUpdates = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        latitudeText = (TextView) findViewById(R.id.latitude_text);
        longitudeText = (TextView) findViewById(R.id.longitude_text);
        accuracyText = (TextView) findViewById(R.id.accuracy_text);
        totalDistanceText = (TextView) findViewById(R.id.total_distance_text);
        startStopButton = (Button) findViewById(R.id.start_stop_button);

        gps = new GPS(this);

        // Start/Stop button listener
        startStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                toggleLocationUpdates();
            }
        });
    }

    @Override
    protected void onStart(){

        super.onStart();

        gps.connectGoogleApiClient();
    }

    @Override
    protected void onStop(){

        super.onStop();

        gps.disconnectGoogleApiClient();
    }

    @Override
    protected void onResume(){

        super.onResume();

        gps.resumeLocationUpdates();
    }

    @Override
    protected void onPause(){

        super.onPause();

        gps.pauseLocationUpdates();
    }

    /**
     * Implement onLocationUpdated(Location) method from OnLocationUpdatedListener
     *
     * Updates the longitude and latitude textViews
     */
    public void onLocationUpdated(Location location){

        longitudeText.setText(getString(R.string.longitude) +
                " " + String.valueOf(location.getLongitude()));

        latitudeText.setText(getString(R.string.latitude) +
                " " + String.valueOf(location.getLatitude()));

        accuracyText.setText("Accuracy: " + String.valueOf(location.getAccuracy() * 3.28084f)
                + " Feet");
    }

    /**
     * Implement onDistanceUpdated(Location) method from OnLocationUpdatedListener
     *
     * Updates the totalDistance textView
     */
    public void onDistanceUpdated(double totalDistance){

        totalDistanceText.setText(getString(R.string.total_distance) +
                " " + String.valueOf(totalDistance) + " Feet");
    }

    /**
     * Turn location updates on/off
     */
    private void toggleLocationUpdates(){

        // If currently not getting location updates,
        // then make changes to begin getting location updates
        // else make changes to stop getting location updates
        if(!isRequestingLocationUpdates){

            isRequestingLocationUpdates = true;

            // reset total distance traveled to 0
            gps.resetDistance();

            // Change text of button to 'Stop'
            startStopButton.setText(getString(R.string.stop_button_label));

            // Start location updates
            gps.startLocationUpdates();

            Log.i(MainActivity.class.getSimpleName(), "Location updates started!");
        } else {

            isRequestingLocationUpdates = false;

            // Change text of button to 'Start'
            startStopButton.setText(getString(R.string.start_button_label));

            // Stop location updates
            gps.stopLocationUpdates();

            Log.i(MainActivity.class.getSimpleName(), "Location updates stopped!");
        }
    }
}
