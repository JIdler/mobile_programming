package com.joeyco.canvasfun;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity {

    Bitmap bitmap;
    Canvas canvas;
    Paint paint;
    RectF rect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Paint paint = new Paint();
        paint.setColor(Color.BLUE);

        Bitmap bitmap = Bitmap.createBitmap(480,800, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        canvas.drawArc(0,0,400,400,20,180,true,paint);

        RelativeLayout layout = (RelativeLayout) findViewById(R.id.canvas);
        layout.setBackground(new BitmapDrawable(getResources(),bitmap));
    }
}
