package com.joeyco.colorpicker;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Joey Idler
 * ColorPicker app
 *
 * ColorBox class is a custom view that fills a rectangle
 * with a RGB color, for use with a color finder/picker
 */
public class ColorBox extends View {

    private Paint paint;
    private int r = 0, g = 0, b = 0; // red, green, blue values

    // ColorBox constructor
    public ColorBox(Context context, AttributeSet attrs) {

        super(context, attrs);

        // set paint color to black (0,0,0)
        paint = new Paint();
        paint.setColor(Color.rgb(r,g,b));
    }

    // set value for red
    // then update and redraw canvas
    public void setRed(int r){

        this.r = r;

        setRGB();
    }

    // set value for green
    // then update and redraw canvas
    public void setGreen(int g){

        this.g = g;

        setRGB();
    }

    // set value for blue
    // then update and redraw canvas
    public void setBlue(int b){

        this.b = b;

        setRGB();
    }

    public int getRed(){

        return r;
    }

    public int getGreen(){

        return g;
    }

    public int getBlue(){

        return b;
    }

    private void setRGB(){
        paint.setColor(Color.rgb(r,g,b));

        // causes onDraw to be called, which redraws the canvas
        this.invalidate();
    }

    @Override
    public void onDraw(Canvas canvas){

        // paint entire canvas with current RGB values
        float left = 0.0f;
        float top = 0.0f;
        float right = canvas.getWidth();
        float bottom = canvas.getHeight();

        canvas.drawRect(left, top, right, bottom, paint);
    }
}
