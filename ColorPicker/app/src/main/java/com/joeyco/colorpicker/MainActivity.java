package com.joeyco.colorpicker;

/**
 * Joey Idler
 * ColorPicker app
 *
 * Implements 3 sliders, which are used to choose
 * red, green, or blue values. The corresponding RGB color
 * is then displayed in a rectangle below the sliders
 */

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private SeekBar redSeekBar, greenSeekBar, blueSeekBar; // seekbar views
    private ColorBox colorBox; // custom colorBox view
    private TextView redLabel, greenLabel, blueLabel; // labels for seekbars

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initialize seekbars
        redSeekBar = (SeekBar) findViewById(R.id.redSeekBar);
        greenSeekBar = (SeekBar) findViewById(R.id.greenSeekBar);
        blueSeekBar = (SeekBar) findViewById(R.id.blueSeekBar);

        // initialize colorBox
        colorBox = (ColorBox) findViewById(R.id.colorBox);

        // initialize labels
        redLabel = (TextView) findViewById(R.id.redLabel);
        greenLabel = (TextView) findViewById(R.id.greenLabel);
        blueLabel = (TextView) findViewById(R.id.blueLabel);

        // Set listeners for seekbars

        redSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int r, boolean b) {

                // modify the label for this seekbar
                redLabel.setText("Red: " + r);

                // set red value
                colorBox.setRed(r);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        greenSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int g, boolean b) {

                // modify the label for this seekbar
                greenLabel.setText("Green: " + g);

                // set green value
                colorBox.setGreen(g);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        blueSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int blue, boolean b) {

                // modify the label for this seekbar
                blueLabel.setText("Blue: " + blue);

                // set blue value
                colorBox.setBlue(blue);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        // check if activity was started from a ACTION_COLOR intent
        // if so, add new button
        Intent isIntent = getIntent();

        if(isIntent.getAction().equals("com.joeyco.colorpicker.ACTION_COLOR")){

            addButton();
        }

    }

    private void addButton(){

        final int select_button = 1234;

        RelativeLayout rl = (RelativeLayout) findViewById(R.id.mainLayout);
        RelativeLayout.LayoutParams buttonParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);

        Button selectButton = new Button(this);
        selectButton.setText("Select Color");
        selectButton.setId(select_button);

        buttonParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        buttonParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        selectButton.setLayoutParams(buttonParams);

        rl.addView(selectButton);

        // Move bottom of colorBox above selectButton
        RelativeLayout.LayoutParams boxParams = (RelativeLayout.LayoutParams) colorBox.getLayoutParams();
        boxParams.addRule(RelativeLayout.ABOVE,select_button);
        colorBox.setLayoutParams(boxParams);

        // setup onClick listener for selectButton
        selectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = getIntent();
                intent.putExtra("red", colorBox.getRed());
                intent.putExtra("green", colorBox.getGreen());
                intent.putExtra("blue", colorBox.getBlue());

                setResult(RESULT_OK, intent);

                finish();
            }
        });
    }

}
