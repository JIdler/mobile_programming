package com.cornez.touchgesturesexperiment2;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;

public class GameView extends View {

    private Paint paint;
    private Circle mCircle;

    public GameView(Context context) {
        super(context);

        //CREATE A CIRCLE AND SET THE PAINT, RADIUS AND X, Y LOCATION
        mCircle = new Circle();

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.rgb(126, 79, 43));
        mCircle.setPaint(paint);

        mCircle.setRadius(100);
        mCircle.setX(400);
        mCircle.setY(300);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        //TASK 1: FILL THE BACKGROUND OF THE CANVAS
        canvas.drawRGB(248, 232, 198);

        //TASK 2: DRAW THE CIRCLE
        canvas.drawCircle(mCircle.getX(), mCircle.getY(),
                mCircle.getRadius(), mCircle.getPaint());

    }

    public void update() {

        invalidate();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //TASK 1:  IDENTIFY THE TOUCH ACTION BEING PERFORMED
        int touchAction = event.getActionMasked();

        //TASK 2:  RESPOND TO TWO POSSIBLE TOUCH EVENTS
        switch (touchAction) {
            case MotionEvent.ACTION_DOWN:
                mCircle.setRadius(300);
                break;
            case MotionEvent.ACTION_UP:
                mCircle.setRadius(75);
                break;
            case MotionEvent.ACTION_MOVE:
                mCircle.setRadius(300);
                mCircle.setX((int) event.getX());
                mCircle.setY((int) event.getY());
                break;
        }

        //TASK 3: INVALIDATE THE  VIEW
        invalidate();

        //TASK 4: RETURNS A TRUE AFTER HANDLING THE TOUCH ACTION EVENT
        return true;
    }
}
