package com.cornez.touchgesturesexperiment2;

import android.graphics.Paint;

public class Circle {


    private Paint mPaint;
    private int mRadius;
    private int mX;
    private int mY;

    public void setPaint(Paint paint) {
        mPaint = paint;
    }
    public Paint getPaint() {
        return mPaint;
    }

    public void setRadius(int radius) {
        mRadius = radius;
    }
    public int getRadius() {
       return  mRadius;
    }

    public void setX(int x) {
        mX = x;
    }
    public int getX() {
        return  mX;
    }
    public void setY(int y) {
        mY = y;
    }
    public int getY() {
        return  mY;
    }
}
