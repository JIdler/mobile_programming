package com.cornez.touchgesturesexperiment3;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.widget.ImageView;

public class ZoomImageView extends ImageView{

    // ACTION CODES FOR USER MODE
    static final int NONE = 0;  //NO ACTIVITY
    static final int MOVE = 1;  //MOVING THE PHOTO
    static final int ZOOM = 2;  //ZOOMING THE PHOTO
    int mode = NONE;

    private Matrix matrix;

    // ZOOM ATTRIBUTES
    private PointF last = new PointF();
    private PointF start = new PointF();
    private float minScale = 1f;
    private float maxScale = 3f;
    private float[] m;

    private int viewWidth;
    private int viewHeight;
    private static final int CLICK = 3;
    private float saveScale = 1f;
    private float origWidth;
    private float origHeight;

    private ScaleGestureDetector mScaleDetector;

    public ZoomImageView(Context context) {
        super(context);

        mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());
        matrix = new Matrix();
        m = new float[9];
        setImageMatrix(matrix);
        setScaleType(ScaleType.MATRIX);
    }

    public void setMaxZoom(float x) {
        maxScale = x;
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            mode = ZOOM;
            return true;
        }

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            float mScaleFactor = detector.getScaleFactor();
            float origScale = saveScale;
            saveScale *= mScaleFactor;
            if (saveScale > maxScale) {
                saveScale = maxScale;
                mScaleFactor = maxScale / origScale;
            } else if (saveScale < minScale) {
                saveScale = minScale;
                mScaleFactor = minScale / origScale;
            }

            if (origWidth * saveScale <= viewWidth || origHeight * saveScale <= viewHeight)
                matrix.postScale(mScaleFactor, mScaleFactor, viewWidth / 2, viewHeight / 2);
            else
                matrix.postScale(mScaleFactor, mScaleFactor, detector.getFocusX(), detector.getFocusY());

            fixTrans();
            return true;
        }
    }

    void fixTrans() {
        matrix.getValues(m);
        float transX = m[Matrix.MTRANS_X];
        float transY = m[Matrix.MTRANS_Y];

        float fixTransX = getFixTrans(transX, viewWidth, origWidth * saveScale);
        float fixTransY = getFixTrans(transY, viewHeight, origHeight * saveScale);

        if (fixTransX != 0 || fixTransY != 0)
            matrix.postTranslate(fixTransX, fixTransY);
    }

    float getFixTrans(float trans, float viewSize, float contentSize) {
        float minTrans, maxTrans;

        if (contentSize <= viewSize) {
            minTrans = 0;
            maxTrans = viewSize - contentSize;
        } else {
            minTrans = viewSize - contentSize;
            maxTrans = 0;
        }

        if (trans < minTrans)
            return -trans + minTrans;
        if (trans > maxTrans)
            return -trans + maxTrans;
        return 0;
    }

    float getFixDragTrans(float delta, float viewSize, float contentSize) {
        if (contentSize <= viewSize) {
            return 0;
        }
        return delta;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        viewWidth = MeasureSpec.getSize(widthMeasureSpec);
        viewHeight = MeasureSpec.getSize(heightMeasureSpec);

        if (saveScale == 1) {
            //TASK 2: SCALE THE IMAGE TO FIT THE SCREEN
            float scale;

            Drawable drawable = getDrawable();
            if (drawable == null || drawable.getIntrinsicWidth() == 0 || drawable.getIntrinsicHeight() == 0)
                return;
            int bmWidth = drawable.getIntrinsicWidth();
            int bmHeight = drawable.getIntrinsicHeight();

            float scaleX = (float) viewWidth / (float) bmWidth;
            float scaleY = (float) viewHeight / (float) bmHeight;
            scale = Math.min(scaleX, scaleY);
            matrix.setScale(scale, scale);

            // TASK 3: PLACE THE IMAGE IN THE CENTER OF THE SCREEN
            float centerX = (float) viewWidth - (scale * (float) bmWidth);
            float centerY = (float) viewHeight - (scale * (float) bmHeight);
            centerX /= (float) 2;
            centerY /= (float) 2;


            matrix.postTranslate(centerX, centerY);

            origWidth = viewWidth - 2 * centerX;
            origHeight = viewHeight - 2 * centerY;
            setImageMatrix(matrix);
        }
        fixTrans();
    }


    //************** onTouchEvent **************************
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mScaleDetector.onTouchEvent(event);
        PointF current = new PointF(event.getX(), event.getY());

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                last.set(current);
                start.set(last);
                mode = MOVE;
                break;

            case MotionEvent.ACTION_MOVE:
                if (mode == MOVE) {
                    float distanceX = current.x - last.x;
                    float distanceY = current.y - last.y;
                    float fixTransX = getFixDragTrans(distanceX, viewWidth, origWidth * saveScale);
                    float fixTransY = getFixDragTrans(distanceY, viewHeight, origHeight * saveScale);
                    matrix.postTranslate(fixTransX, fixTransY);
                    fixTrans();
                    last.set(current.x, current.y);
                }
                break;

            case MotionEvent.ACTION_UP:
                mode = NONE;
                int xDiff = (int) Math.abs(current.x - start.x);
                int yDiff = (int) Math.abs(current.y - start.y);
                if (xDiff < CLICK && yDiff < CLICK)
                    performClick();
                break;

            case MotionEvent.ACTION_POINTER_UP:
                mode = NONE;
                break;
        }
        //SET THE IMAGE AND INVALIDATE THE COMPLETE VIEW
        setImageMatrix(matrix);
        invalidate();

        //RETURN A TRUE TO INDICATE THE EVENT WAS HANDLED
        return true;
    }
}
