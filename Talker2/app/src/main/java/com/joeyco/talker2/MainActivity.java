package com.joeyco.talker2;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public final int REQUEST_ENABLE = 3313;
    public final int REQUEST_LOCATION = 4444;
    public final String TCLIENT = "Talker Client";
    public final String TSERVER = "Talker SERVER";

    private BluetoothAdapter bluetoothAdapter;

    private boolean isOff = true;

    private ListView devicesList;
    Button scanButton;
    private ArrayAdapter<String> arrayAdapter;

    private final BroadcastReceiver receiver =
            new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {

                    Log.i(TCLIENT, "onReceive() -- starting   <<<<--------------------");
                    String action = intent.getAction();
                    if (BluetoothDevice.ACTION_FOUND.equals(action)){

                        BluetoothDevice device = intent
                                .getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                        Log.i(TCLIENT, device.getName() + "\n" + device);

                        arrayAdapter.add(device.getName() + "\n" + device.getAddress());
                        arrayAdapter.notifyDataSetChanged();
                    }
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        devicesList = (ListView)findViewById(R.id.devices_list);
        arrayAdapter =
                new ArrayAdapter<String>(MainActivity.this,
                        android.R.layout.simple_list_item_1);
        devicesList.setAdapter(arrayAdapter);

        checkPermissions();
        buttonSetup();
    }

    private void checkPermissions(){

        Log.i("checkPermissions()-", "Start check permissions");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Check Permissions Now
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION);
        }
        Log.i("checkPermissions()-", "End check permissions");
    }

    private void buttonSetup(){

        Log.i("buttonSetup()-", " Setting up scan button" );
        scanButton = (Button) findViewById(R.id.scan_button);

        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.i("onClick()-", " Scan button clicked");

                if (isOff) {

                    IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
                    registerReceiver(receiver, filter);
                    arrayAdapter.clear();
                    bluetoothAdapter.startDiscovery();
                    isOff = false;
                    Log.i("onClick()-", " Discovery Started");
                } else {

                    bluetoothAdapter.cancelDiscovery();
                    isOff = true;
                    Log.i("onClick()-", " Discovery Stopped");
                }
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        Log.i("Bluetooth Test", "onResume()");

        if (bluetoothAdapter == null){

            Toast.makeText(getBaseContext(),
                    "No Bluetooth on this device", Toast.LENGTH_LONG).show();
        } else if (!bluetoothAdapter.isEnabled()){

            Log.i("Bluetooth Test", "enabling Bluetooth");
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE);
        }

        Log.i("Bluetooth Test", "End of onResume()");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.i("Bluetooth Test", "onActivityResult(): requestCode = " + requestCode);
        if (requestCode == REQUEST_ENABLE){
            if (resultCode == RESULT_OK){
                Log.i("Bluetooth Test", "  --    Bluetooth is enabled");
            }
        }
    }

    @Override
    protected void onStop(){
        super.onStop();

        bluetoothAdapter.cancelDiscovery();
        unregisterReceiver(receiver);
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();

        bluetoothAdapter.cancelDiscovery();
    }


}
