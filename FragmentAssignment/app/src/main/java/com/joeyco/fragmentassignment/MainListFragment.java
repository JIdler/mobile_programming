package com.joeyco.fragmentassignment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

/**
 *
 *  Joey Idler
 *
 */
public class MainListFragment extends Fragment {

    private OnItemSelectedListener listener;
    private Button button1, button2, button3, button4;
    private Fragment makaluFragment, kangchenjungaFragment, lhotseFragment, nangaParbatFragment;

    private OnClickListener ItemChangeListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            updateFragment(v.getId());
        }
    };

    // Empty Default Constructor
    public MainListFragment(){
    }

    public interface OnItemSelectedListener {
        public void onItemSelected(Fragment fragment);
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        makaluFragment = new MakaluFragment();
        kangchenjungaFragment = new KangchenjungaFragment();
        lhotseFragment = new LhotseFragment();
        nangaParbatFragment = new NangaParbatFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        //inflate layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main_list, container, false);

        // set up buttons and their listeners
        button1 = (Button) view.findViewById(R.id.button1);
        button1.setOnClickListener(ItemChangeListener);

        button2 = (Button) view.findViewById(R.id.button2);
        button2.setOnClickListener(ItemChangeListener);

        button3 = (Button) view.findViewById(R.id.button3);
        button3.setOnClickListener(ItemChangeListener);

        button4 = (Button) view.findViewById(R.id.button4);
        button4.setOnClickListener(ItemChangeListener);

        return view;
    }

    @Override
    public void onAttach(Context context){

        super.onAttach(context);

        try {

            listener = (OnItemSelectedListener) context;
        } catch(ClassCastException e){

            throw new ClassCastException(context.toString() +
                    " must implement" +
                    "MainListFragment.OnItemSelectedListener");
        }
    }

    private void updateFragment(int id){

        if(id == button1.getId()){

            listener.onItemSelected(makaluFragment);
            return;
        } else if(id == button2.getId()){

            listener.onItemSelected(kangchenjungaFragment);
            return;
        } else if(id == button3.getId()){

            listener.onItemSelected(lhotseFragment);
            return;
        } else if(id == button4.getId()){

            listener.onItemSelected(nangaParbatFragment);
            return;
        }

        Log.w("Warning: ", "Error in updateFragment()");
    }
}
