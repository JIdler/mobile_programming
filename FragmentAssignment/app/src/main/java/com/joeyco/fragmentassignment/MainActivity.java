package com.joeyco.fragmentassignment;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

/**
 *
 * Joey Idler
 *
 */

public class MainActivity extends AppCompatActivity
                                    implements MainListFragment.OnItemSelectedListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState == null){

            getSupportFragmentManager().beginTransaction()
                                        .add(R.id.fragment_container, new MainListFragment())
                                        .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        if(item.getItemId() == R.id.backActionItem) {

            getSupportFragmentManager().popBackStack();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onItemSelected(Fragment fragment){

        getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.fragment_container,fragment)
                                    .addToBackStack(null)
                                    .commit();
    }
}
