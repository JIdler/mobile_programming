package com.joeyco.colorblender;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Joey Idler
 *
 * ColorBox View
 *
 * ColorBox class is a custom view that fills a rectangle
 * with a RGB color
 */
public class ColorBox extends View {

    private Paint paint;
    private int r = 0, g = 0, b = 0;

    // ColorBox constructor
    public ColorBox(Context context, AttributeSet attrs){

        super(context, attrs);

        // set paint color to black (0,0,0)
        paint = new Paint();
        paint.setColor(Color.rgb(0,0,0));
    }

    public int getR(){

        return r;
    }

    public int getG() {

        return g;
    }

    public int getB() {

        return b;
    }

    public void blendColors(ColorBox box1, ColorBox box2, float lRatio, float rRatio){

        r = (int)((box1.getR() * lRatio) + (box2.getR() * rRatio));
        g = (int)((box1.getG() * lRatio) + (box2.getG() * rRatio));
        b = (int)((box1.getB() * lRatio) + (box2.getB() * rRatio));

        paint.setColor(Color.rgb(r,g,b));
        this.invalidate();
    }

    public void setColor(int r, int g, int b){

        this.r = r;
        this.g = g;
        this.b = b;

        paint.setColor(Color.rgb(r,g,b));
        this.invalidate();
    }

    @Override
    public void onDraw(Canvas canvas){

        // paint entire canvas with current RGB values
        float left = 0.0f;
        float top = 0.0f;
        float right = canvas.getWidth();
        float bottom = canvas.getHeight();

        canvas.drawRect(left, top, right, bottom, paint);
    }
}
