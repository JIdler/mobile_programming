package com.joeyco.colorblender;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;

/**
 * Joey Idler
 *
 * ColorBlender app
 *
 * Users can choose 2 different colors using the ColorPicker app
 * and then the colors can be blended using a slider
 */



public class MainActivity extends AppCompatActivity {

    static final String ACTION_COLOR = "com.joeyco.colorpicker.ACTION_COLOR";
    static final int LEFT_REQUEST_CODE = 1, RIGHT_REQUEST_CODE = 2;

    private ColorBox leftColorBox, rightColorBox, blendedColorBox;

    private float leftRatio = 1.0f, rightRatio = 0.0f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initialize ColorBox's
        leftColorBox = (ColorBox) findViewById(R.id.left_color);
        rightColorBox = (ColorBox) findViewById(R.id.right_color);
        blendedColorBox = (ColorBox) findViewById(R.id.blended_color);

        // initialize Buttons
        Button leftButton = (Button) findViewById(R.id.select_color_button_left);
        Button rightButton = (Button) findViewById(R.id.select_color_button_right);

        // initialize seekbar
        SeekBar slider = (SeekBar) findViewById(R.id.seekBar);

        // on seekbar change listener
        slider.setOnSeekBarChangeListener(blendColors);

        // on click button handlers for getting getting rgb values from ColorPicker
        leftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                activateColorPicker(LEFT_REQUEST_CODE);
            }
        });

        rightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                activateColorPicker(RIGHT_REQUEST_CODE);
            }
        });

    }

    // on seekbar change handler for blending colors
    private SeekBar.OnSeekBarChangeListener blendColors = new SeekBar.OnSeekBarChangeListener(){

        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            leftRatio = (100.0f - i) / 100.0f;
            rightRatio = i / 100.0f;

            blendedColorBox.blendColors(leftColorBox, rightColorBox, leftRatio, rightRatio);

        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    private void activateColorPicker(final int requestCode) {

        Intent colorIntent = new Intent(ACTION_COLOR);

        startActivityForResult(colorIntent, requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        if(requestCode == LEFT_REQUEST_CODE && resultCode == RESULT_OK){

            int r = data.getIntExtra("red",0);
            int g = data.getIntExtra("green",0);
            int b = data.getIntExtra("blue",0);

            leftColorBox.setColor(r,g,b);

        }
        else if (requestCode == RIGHT_REQUEST_CODE && resultCode == RESULT_OK){

            int r = data.getIntExtra("red",0);
            int g = data.getIntExtra("green",0);
            int b = data.getIntExtra("blue",0);

            rightColorBox.setColor(r,g,b);

        }

        blendedColorBox.blendColors(leftColorBox,rightColorBox,leftRatio,rightRatio);
    }
}
